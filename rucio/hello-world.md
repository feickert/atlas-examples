# Rucio basics

## Query datasets

Use `rucio list-dids` to list the datasets that match the input search string

```console
$ rucio list-dids user.feickert.debug_00a98824-eb03-4d75-9df2-464e8318c37f*
+--------------------------------------------------------------------------------------+-----------------+
| SCOPE:NAME                                                                           | [DID TYPE]      |
|--------------------------------------------------------------------------------------+-----------------|
| user.feickert:user.feickert.debug_00a98824-eb03-4d75-9df2-464e8318c37f.log.426829045 | DIDType.DATASET |
+--------------------------------------------------------------------------------------+-----------------+
```

## Download datasets

Use `rucio download` to download the datasets that you've found

```console
$ rucio download user.feickert:user.feickert.debug_00a98824-eb03-4d75-9df2-464e8318c37f.log.426829045
```

## Replicate datasets

You can efficiently copy datasets from one [Rucio Storage Element][Rucio Storage Element] (RSE) to another with the [`rucio add-rule` command][rucio add-rule].
You can use regex expressions as inputs to `rucio list-rses` to determine viable RSEs to give to `rucio add-rule`.

**Example**:

```console
$ rucio add-rule mc16_13TeV:mc16_13TeV.364677.Sherpa_CT10_CT14nnlo_CSShower_2to2jets_JZ1W.deriv.DAOD_JETM1.e6997_e5984_s3126_r10201_r10210_p4346 1 MWT2_UC_SCRATCHDISK
```

Once you've started a replication you can query the status of it with `rucio list-rules`, either with the `id` that was given by `rucio add-rule`

```console
$ rucio list-rules --id <id>
```

or via your account name

```console
$ rucio list-rules --account "${RUCIO_ACCOUNT}"
```

### Replicating multiple datasets

As `rucio add-rule` doesn't accept pattern matching and requires a full dataset id, to replicate multiple datasets first get all the datasets into a single file and then iterate over the file:

**Command line example**:

```console
$ while IFS= read -r did; do rucio add-rule "${did}" 1 MWT2_UC_SCRATCHDISK; done < datasets_to_replicate.txt
```

**Bash example**:

```bash
#!/bin/bash

while IFS= read -r did; do
    rucio add-rule "${did}" 1 MWT2_UC_SCRATCHDISK
done < datasets_to_replicate.txt
```

[Rucio Storage Element]: https://rucio.cern.ch/documentation/rucio_storage_element/
[rucio add-rule]: https://rucio.cern.ch/documentation/replication_rules_examples
[RSE expressions]: https://rucio.cern.ch/documentation/rse_expressions/
