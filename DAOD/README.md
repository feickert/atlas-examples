# DAOD basics

## Check branch names

If you have a DAOD and would like to query it for all the branch names that are contained in it you can do so with `AthAnalysis`'s `checkFile.py` command.

```console
$ checkFile.py --help
Usage: checkFile.py [options] [-f] my.file.pool

Options:
  -h, --help            show this help message and exit
  -f FILENAME, --file=FILENAME
                        The path to the POOL file to analyze
  -d, --detailed-dump   Switch to activate or not a detailed dump of each
                        TTree in the POOL file
  --sort-fct=SORTFCTNAME
                        Sorting function used to list containers (allowed are:
                        diskSize, memSize, name)
  -o OUTFILENAME, --output=OUTFILENAME
                        Name of the output file which will contain the
                        informations gathered during checkFile processing.
                        These informations will be stored into a python-shelve
                        file.
  --fast                Switch to enable the fast mode of checkFile.py (memory
                        size will not be accurate -AT ALL-)
  --detailed-branch-size
                        Switch to enable a very detailed computation of the
                        branch sizes (computed from the basket length) [SLOW]
```

This first requires downloading a pool file from the DAOD with `rucio`

```console
$ rucio download --nrandom 1 <scope:DID NAME>
```

then setting up an `AthAnalysis` release

```console
$ asetup AthAnalysis,<your release number>
```

and then running `checkFile.py` on the DAOD file

```console
$ checkFile.py <path to downloaded DAOD pool file>
```

This will by default print out all the following 7 columns of information for each branch in the DAOD:

* Mem Size
* Disk Size
* Size/Evt
* MissZip/Mem
* items
* (X) (X=Tree|Branch)
* Container Name

This information can then be filtered down with various tools as shown in the `check_branch_names.sh` example.
