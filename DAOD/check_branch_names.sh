#!/bin/bash

function print_and_run () {
    printf "\n# %s\n" "${1}"
    eval $(echo "$1")
}

if [ "$(basename -- $0)" = "$(basename ${BASH_SOURCE})" ]; then
    # running in subshell
    # Define setupATLAS and asetup
    ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    function setupATLAS {
        source "${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"
    }
fi

baseDir=${PWD}

print_and_run "setupATLAS"
print_and_run "lsetup python git asetup"

print_and_run "asetup AthAnalysis,21.2.71"

file_base_path="/scratch/feickert/mc16_13TeV.503740.MGPy8EG_A14NNPDF23LO_GG_rpvHF_1400_200.deriv.DAOD_SUSY5.e8258_e7400_a875_r10201_r10210_p4174"
file_name=DAOD_SUSY5.*.pool.root.1
print_and_run "checkFile.py ${file_base_path}/${file_name} > branch_names.txt"
print_and_run "grep Jets branch_names.txt | tee jet_containers.txt"
printf "\n# grep GhostTrack jet_containers.txt | awk '{print "'$(NF)'"}' > ghost_track_jet_containers.txt\n"
grep GhostTrack jet_containers.txt | awk '{print $(NF)}' > ghost_track_jet_containers.txt
